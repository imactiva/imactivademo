﻿using UnityEngine;
using System.Collections;

public class KeyboardAwsomeDetector : MonoBehaviour
{
	[SerializeField]
	public float walkSpeed = 8;

	[SerializeField]
	public float jumpSpeed = 4;
	
	float axisHorizontal = 0;

	Rigidbody2D rigidBody; 

	// Use this for initialization
	void Start ()
	{
		rigidBody = this.gameObject.GetComponent<Rigidbody2D> ();
	}
	
	//All Rigid Bodies Goes Here
	void FixedUpdate()
	{
		rigidBody.velocity = new Vector2 (axisHorizontal * walkSpeed, rigidBody.velocity.y);
		
	}
	// Update is called once per frame
	void Update ()
	{
		
		//Check Keyboar Press
		axisHorizontal = Input.GetAxis ("Horizontal");

		//Nice Ways to move things when not depending of rigidbody
		//Vector3 v3 = this.gameObject.transform.position;
		//v3.x += axisHorizontal;
		//this.gameObject.transform.position = v3;
	}
	

	
}
