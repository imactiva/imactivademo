# README #

This readme will guide you into creating a simple unity scene. enjoy

### What is this repository for? ###

* Unity basic
* Versioning
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Who do I talk to? ###

* Repo owner or admin
* Ricardo Gonzalez


# Unity #

From here, we start learning unity, but first ...

### Developer Tools ###

* SourceTree and Bitbucket Account
* Sublime Text
* Git (command line for your favorite OS)
* Unity and User Account


### Seupt Unity To Play Nice With Git ###


You need to do these settings:

* Switch to Visible Meta Files in Edit → Project Settings → Editor → Version Control Mode.

![Alt text](Tutorial/Project%20Setups/GitSetup01.png "Version Control")


* Switch to Force Text in Edit → Project Settings → Editor → Asset Serialization Mode.

![Alt text](Tutorial/Project%20Setups/GitSetup01.png "Serialization")



### Steps To Build A Successfully Unity Project ###

* Folder Structure
* todo ..

### Folder Structure ###

```
Assets
	_Scenes
	Animations
	Imgs
	Physics Materials
	Scripts
```


### GameObjects ###

* Transform
* Sprite Render
* Script
* Rigid Body 2D,3D
* Box Colliders

### Sprite Renderer ###

The Sprite Renderer component lets you display images as Sprites for use in both 2D and 3D scenes.

Add it to a GameObject via the Components menu (Component > Rendering > Sprite Renderer or alternatively, you can just create a GameObject directly with a Sprite Renderer already attached (menu: GameObject > 2D Object > Sprite).

* Import Sprites into the Imgs
* Add Sprite Render
* Add Sprite (BackgroundGreyGridSprite)

![Alt text](Tutorial/GameObjects/SpriteRender.png "Sprite Render")


### Rigidbody 2D ###

Add to enemy, and plataform
menu: GameObject -> Physics 2D -> Rigidbody 2D

then remove it from plataform (see what happens)


Note: Dont Forget to reset the transform to zeros.

![Alt text](Tutorial/GameObjects/Reset.png "Reset Object Positon")


### Colliders ###

Add to enemy, and plataform
menu: GameObject -> Physics 2D -> Box Colliders 2D


### Materials ###

Pull To Get The Ball Material, Added To The Enemy


### Script ###

* Create script DetectHitGround and add it to the enemy
* Move the script into the correct folder
* Tag the Plataform as "Ground"


### Audio ###

* Create AudioSource
* Add Audio
* Serialize the audio, and add the audio to this.


```
public class DetectHitGround : MonoBehaviour {

	float counter = 0;

	[SerializeField]
	private AudioClip shootSound;

```

### Player / Keyboard ###

* Create Player
* Tag Player, remove things like material body, audio, etc


### World Loop Sound ###

* GameObject Added with source audio world, low volumen, loop

![Alt text](Tutorial/GameObjects/GhostSound.png "Reset Pivots")


### Bartolo Simple Animation Talk ###

* Importar Bartolo Imgs/Animations
* animacion_bartolo_cabezon and select Multiple Sprite Mode (Atlas/SpriteSheet)
	- Slice, cmd + backspace deletes square of an eye., stretch the other square eyes to join both.
	- Close then apply, check animation pressing the arrow
* repeat for all
* for bartolo_brazo_altos change the pivot
* brazo_bartolo change pivot too
* Create Bartolo Game Object, reset transform to 0,0,0. use reset.
* bartolo_frente_manos as subchild of BodyMain
* create Bartolo Animator,cmd 6 call it idle
* create new clip, call it talk
* select talk, at time 0.5 change of sprite with bartolo_frente_manos_noface
* add another GameObject to Bartolo, animacion_bartolo_cabezon_* (ojos), change name
* remove rec, and chage size to fit sizes of the another eyes
* add bartolo mouth too, add GameObject change name "mouth" change to pistion
* record
* add game objects to timeline, check the animation.

Note: Move Pivots to shoulder, etc. Dont forget to setup pivot view, not center.

![Alt text](Tutorial/GameObjects/Pivot.png "Reset Pivots")


Tips: 
	Use Shit + Traslate to Keep Aspect Ratio
	Sprite Render, Uncheck to hide the game object

	Put sprite to eyes and mouth on the line time, and then take them off to blink.

	Key Frame can helps to strech animation.
	Curves for easy-in-out

Note: How To Create The Atlases.

![Alt text](Tutorial/GameObjects/SpriteEditor.png "Split Sprites Animations")

### Bartolo Simple Animation Arm ###
	
* add a new clip call it arm
* move timeline to 0, this makes recordig to rec. select BodyMain change sprite to empty bartolo



### Links ###


[ Main Documentation ](URL)http://docs.unity3d.com/Manual/index.html
	
[ Interface Overview ](URL)http://unity3d.com/learn/tutorials/modules/beginner/editor/interface-overview?playlist=17090






